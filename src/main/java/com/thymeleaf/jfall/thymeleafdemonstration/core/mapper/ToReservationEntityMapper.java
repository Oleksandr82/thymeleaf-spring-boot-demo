package com.thymeleaf.jfall.thymeleafdemonstration.core.mapper;

import org.springframework.stereotype.Component;

import com.thymeleaf.jfall.thymeleafdemonstration.api.model.CreateReservationFormData;
import com.thymeleaf.jfall.thymeleafdemonstration.data.Employee;
import com.thymeleaf.jfall.thymeleafdemonstration.data.Reservation;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class ToReservationEntityMapper {

    public Reservation map(final CreateReservationFormData formData, final Employee employee) {
        final Reservation entity = new Reservation();
        entity.setDate(formData.getDate());
        entity.setEmployee(employee);
        entity.setLocation(formData.getLocation());
        entity.setStartTime(formData.getStartTime());
        entity.setEndTime(formData.getEndTime());
        return entity;
    }

}
