package com.thymeleaf.jfall.thymeleafdemonstration;

import java.time.LocalTime;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.github.javafaker.Faker;
import com.github.javafaker.Name;
import com.thymeleaf.jfall.thymeleafdemonstration.data.Employee;
import com.thymeleaf.jfall.thymeleafdemonstration.data.EmployeeEntityRepository;
import com.thymeleaf.jfall.thymeleafdemonstration.data.Reservation;
import com.thymeleaf.jfall.thymeleafdemonstration.data.ReservationEntityRepository;

import lombok.RequiredArgsConstructor;

@Component
@Profile("init-db")
@RequiredArgsConstructor
public class DatabaseInitializer implements CommandLineRunner {

    private final Faker faker = new Faker();
    private final EmployeeEntityRepository employeeEntityRepository;
    private final ReservationEntityRepository reservationEntityRepository;

    @Override
    public void run(String... args) {
        IntStream.range(0, 10)
                .forEach(index -> {
                    final Reservation reservation = buildRandomReservation();
                    System.out.println(reservation);
                    saveReservation(reservation);
                });
    }

    private Reservation buildRandomReservation() {
        final Employee employee = buildRandomEmployee();
        return buildRandomReservation(employee);
    }

    private Reservation buildRandomReservation(final Employee employee) {
        final Reservation entity = new Reservation();
        entity.setEmployee(employee);
        entity.setLocation(buildRandomLocation());
        entity.setDate(faker.date()
                .future(60, TimeUnit.DAYS)
                .toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate());
        entity.setStartTime(buildRandomStartTime());
        entity.setEndTime(buildRandomEndTime());
        return entity;
    }

    private LocalTime buildRandomEndTime() {
        final int hour = faker.options()
                .option(16, 17, 18);
        return LocalTime.of(hour, randomMinutes());
    }

    private int randomMinutes() {
        return faker.options()
                .option(15, 30, 45, 0);
    }

    private LocalTime buildRandomStartTime() {
        final int hour = faker.options()
                .option(7, 8, 9, 10, 11);
        return LocalTime.of(hour, randomMinutes());
    }

    private String buildRandomLocation() {
        return String.format("%s-%d", faker.options()
                .option("A", "B", "C", "D", "E"), faker.number()
                .numberBetween(1000, 2000));
    }

    private Employee buildRandomEmployee() {
        final Name name = faker.name();
        final String firstName = name.firstName();
        final String lastName = name.lastName();
        final Employee entity = new Employee();
        entity.setFirstName(firstName);
        entity.setLastName(lastName);
        entity.setPersonnelNumber((long) faker.number()
                .numberBetween(10000, 99999));
        entity.setEmail(generateEmailAddress(firstName, lastName));
        entity.setPhoneNumber(faker.phoneNumber()
                .phoneNumber());
        return entity;
    }

    private String generateEmailAddress(final String firstName, final String lastName) {
        final String first = StringUtils.remove(firstName.toLowerCase(), "'");
        final String last = StringUtils.remove(lastName.toLowerCase(), "'");
        return String.format("%s@%s.com", first, last);
    }

    private void saveReservation(final Reservation entity) {
        employeeEntityRepository.save(entity.getEmployee());
        reservationEntityRepository.save(entity);
    }
}
