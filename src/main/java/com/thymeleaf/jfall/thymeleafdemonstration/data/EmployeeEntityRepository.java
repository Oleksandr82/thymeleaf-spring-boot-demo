package com.thymeleaf.jfall.thymeleafdemonstration.data;

import org.springframework.data.repository.CrudRepository;

public interface EmployeeEntityRepository extends CrudRepository<Employee, Long> {
}
