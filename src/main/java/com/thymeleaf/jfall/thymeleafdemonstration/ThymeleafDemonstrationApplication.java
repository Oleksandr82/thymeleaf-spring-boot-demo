package com.thymeleaf.jfall.thymeleafdemonstration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication()
public class ThymeleafDemonstrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThymeleafDemonstrationApplication.class, args);
	}

}
